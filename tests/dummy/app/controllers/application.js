import EmberObject from '@ember/object';
import Controller from '@ember/controller';
import { A } from '@ember/array';
import faker from 'faker';

export default Controller.extend({
  tableConfig: null,
  init() {
    this._super(...arguments);

    var object = EmberObject.extend({});

    var tableConfig = object.create();
    var tableTopRows = A([]);

    for (let i = 0; i < 100; i++) {
      var row = object.create();
      let id = faker.random.uuid();
      row.set("id", id);

      let name = A([faker.name.jobTitle(), faker.name.jobTitle(), faker.name.jobTitle(), faker.name.jobTitle()]);
      row.set("courseName",name);
      tableTopRows.push(row);
      row.set("inactive", A(["inactive"]));
    }

    var tableBottomRows = A([]);

    for (let i = 0; i < 100; i++) {
      var row = object.create();
      let id = faker.random.uuid();
      row.set("id", id);

      let name = A([faker.name.jobTitle(), faker.name.jobTitle()]);
      row.set("courseName", name);
      tableBottomRows.push(row);
    }

    var rowArrays = object.create();
    rowArrays.set("tableTopRows", tableTopRows);
    rowArrays.set("tableBottomRows", tableBottomRows);

    tableConfig.set("rowData", rowArrays);

    var tableConfigColumns = A([]);
    var nameFieldConfig = object.create();
    var nameFieldFields = A(["courseName"]);
    nameFieldConfig.set("fields", nameFieldFields);
    nameFieldConfig.set("label", "Course Name");
    nameFieldConfig.set("class", "name-field");
    tableConfigColumns.push(nameFieldConfig);
    var inactiveFieldConfig = object.create();
    var inactiveFieldFields = A(["inactive"]);
    inactiveFieldConfig.set("fields", inactiveFieldFields);
    inactiveFieldConfig.set("label", "Inactive")
    inactiveFieldConfig.set("class", "inactive-field");
    tableConfigColumns.push(inactiveFieldConfig);
    tableConfig.set("columns", tableConfigColumns);
    tableConfig.set("sortFields", A(["courseName"]))
    tableConfig.set("checkedRows", A([]));

    this.set("tableConfig", tableConfig);

  },

  actions: {
    updateChecked(updates){
      this.set("tableConfig.checkedRows", updates);
    }
  }
});
