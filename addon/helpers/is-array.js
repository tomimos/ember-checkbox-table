import { helper } from '@ember/component/helper';

export function isArray(object) {
  return Array.isArray(object);
}

export default helper(isArray);
