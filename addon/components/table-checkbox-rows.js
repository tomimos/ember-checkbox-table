import Component from '@ember/component';
import { computed } from '@ember/object';
import { A } from '@ember/array';

export default Component.extend({
  tableName: 'default',
  tableExpandOffset: 400, // TODO - Debounce this and enhance this logic
  noItemsMessage: "No Items Found",

  init() {
    this._super(...arguments);
    this.handleResize = function() {
      console.log($(window).height());
      $(".scrollable-table").height($(window).height() - this.get('tableExpandOffset'));
    }.bind(this);
    Ember.run.next(this, this.handleResize);
    $(window).on('resize', Ember.run.bind(this, this.handleResize));
  },

  allChecked: computed('checkedRows', 'rowData', function() {
    var rowData = this.get('rowData');
    var allRows = rowData.tableTopRows.concat(rowData.tableBottomRows)
    var checkedRows = this.get('checkedRows');
    return (checkedRows.length === allRows.length) && checkedRows.length;
  }),

  allUnchecked: computed('checkedRows', 'rowData', function() {
    var checkedRows = this.get('checkedRows');
    return !checkedRows.length;
  }),

  updateAllChecked: function() {
    // the action to passed to Component
    this.updateChecked(this.get("checkedRows"));
  },

  actions: {

    syncAllChecked: function() {
      this.updateAllChecked()
    },

    checkAll: function() {
      var rowData = this.get('rowData');
      var allRows = rowData.tableTopRows.concat(rowData.tableBottomRows)
      var newCheckedArray = A([]);
      allRows.forEach(function(item) {
        newCheckedArray.push(item.id)
      });
      this.set("checkedRows", newCheckedArray);
      this.updateAllChecked();
    },

    uncheckAll: function() {
      var newCheckedArray = A([]);
      this.set("checkedRows", newCheckedArray);
      this.updateAllChecked();
    }
  }
});
